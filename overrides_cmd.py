#!/usr/bin/env python

# Example of an overrides command

# Override commands can update the overrides json file
# with new values to be used.  This is a great way to
# update an authentication token that will expire
# during testing.

import backoff
import json
import logging
import logging.config
import os
import requests
import urllib3.exceptions
from typing import Any, Callable, List, Dict, Optional, cast  # noqa


# [1] Store log file in directory indicated by env var CI_PROJECT_DIR
working_directory = os.environ.get('CI_PROJECT_DIR')
overrides_file_name = os.environ.get('DAST_API_OVERRIDES_FILE', 'dast-api-overrides.json')
overrides_file_path = os.path.join(working_directory, overrides_file_name)

# [2] File name must match the pattern: gl-*.log
log_file_path = os.path.join(working_directory, 'gl-user-overrides.log')

# Basic logging configuration
logging_config = {
    "version": 1,
    "loggers": {
        "": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
        }},
    "formatters": {
        "console": {
            "format": "%(asctime)s [%(levelname)-3.3s]: %(message)s",
            "datefmt": "%H:%M:%S",
        }, "file": {
            "format": "%(asctime)s [%(levelname)-3.3s]: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        }},
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "stream": "ext://sys.stdout",
            "formatter": "console",
        }, "file": {
            "class": "logging.FileHandler",
            "filename": log_file_path,
            "level": "DEBUG",
            "formatter": "file",
        }}}

# Set up logger
logging.config.dictConfig(logging_config)


def on_backoff(details):
    if 'wait' not in details or 'tries' not in details:
        logging.info("Request failed, backing off...")
        return
    logging.info(f"Request failed, backing off {details['wait']} seconds afters {details['tries']} tries")


# Use `backoff` decorator to retry in case of transient errors.
@backoff.on_exception(backoff.expo,
                      (requests.exceptions.Timeout,
                       requests.exceptions.ConnectionError,
                       requests.exceptions.RequestException,
                       requests.exceptions.HTTPError,
                       urllib3.exceptions.NewConnectionError,
                       urllib3.exceptions.HTTPError,
                       OSError),
                      on_backoff=on_backoff,
                      max_time=30)
def get_auth_response() -> Dict[str, Any]:
    response = requests.get('http://selenium:8000/token')
    response.raise_for_status()
    return response.json()


# In our example, access token is retrieved from a given endpoint
try:

    # Performs a http request, response sample:
    # { "token" : "b5638ae7-6e77-4585-b035-7d9de2e3f6b3" }
    response_body = get_auth_response()

# If needed specific exceptions can be caught
# requests.ConnectionError                  : A network connection error problem occurred
# requests.HTTPError                        : HTTP request returned an unsuccessful status code. [Response.raise_for_status()]
# requests.ConnectTimeout                   : The request timed out while trying to connect to the remote server
# requests.ReadTimeout                      : The server did not send any data in the allotted amount of time.
# requests.TooManyRedirects                 : The request exceeds the configured number of maximum redirections
# requests.exceptions.RequestException      : All exceptions that related to Requests
except json.JSONDecodeError:
    # logs errors related decoding JSON response
    logging.error('Error, failed while decoding JSON response.', exc_info=True)
    raise
except requests.exceptions.RequestException:
    # logs  exceptions  related to `Requests`
    logging.error('Error, failed while performing HTTP request.', exc_info=True)
    raise
except Exception:
    # logs any other error
    logging.error('Error, unknown error while retrieving access token.', exc_info=True)
    raise

# computes object that holds overrides file content.
# It uses data fetched from request
overrides_data = {
    "headers": {
        "Authorization": f"Token {response_body['token']}"
    }
}

# log entry informing about the file override computation
logging.info("Creating overrides file: %s" % overrides_file_path)

# attempts to overwrite the file
try:
    if os.path.exists(overrides_file_path):
        os.unlink(overrides_file_path)

    # overwrites the file with our updated dictionary
    with open(overrides_file_path, "wb+") as fd:
        fd.write(json.dumps(overrides_data).encode('utf-8'))
except Exception:
    # logs any other error
    logging.error(f'Error, unknown error when overwriting file {overrides_file_path}.', exc_info=True)
    raise

# logs informing override has finished successfully
logging.info("Override file has been updated")

# end
